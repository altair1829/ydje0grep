package jtm.activity09;

import java.util.*;

/*- TODO #1
 * Implement Comparable interface with Order class
 * Hint! Use generic type of comparable items in form: Comparable<Order>
 * 
 * TODO #2 Override/implement necessary methods for Order class:
 * - public Order(String orderer, String itemName, Integer count) â€” constructor of the Order
 * - public int compareTo(Order order) â€” comparison implementation according to logic described below
 * - public boolean equals(Object object) â€” check equality of orders
 * - public int hashCode() â€” to be able to handle it in some hash... collection 
 * - public String toString() â€” string in following form: "ItemName: OrdererName: Count"
 * 
 * 
 
 * Hints:
 * 1. When comparing orders, compare their values in following order:
 *    - Item name
 *    - Customer name
 *    - Count of items
 * If item or customer is closer to start of alphabet, it is considered "smaller"
 * 
 * 2. When implementing .equals() method, rely on compareTo() method, as for sane design
 * .equals() == true, if compareTo() == 0 (and vice versa).
 * 
 * 3. Also Ensure that .hashCode() is the same, if .equals() == true for two orders.
 * 
 */

	
public class Order implements Comparable<Order> {

	String customer; // Name of the customer
	String name; // Name of the requested item
	Integer count;
		
	public Order (String orderer, String itemName, Integer count) {
		this.customer = orderer;
		this.name = itemName;
		this.count = count;
	}

	
	@Override
	public int compareTo(Order o) {
		// at first compare by item name:
		int i = 0;
		if (this.name.compareTo(o.name) < i) {
			i = -1;
		}
		else if (this.name.compareTo(o.name) > i) {
			i = 1;
		}
		else if (this.name.compareTo(o.name) == i) {
			if (this.customer.compareTo(o.customer) < i) {
				i = -1;
			}
			else if (this.customer.compareTo(o.customer) > i) {
				i = 1;
			}
			else if (this.customer.compareTo(o.customer) == 0) {
				if (this.count.compareTo(o.count) < i) {
					i = -1;
				}
				else if (this.count.compareTo(o.count) > i) {
					i = 1;
				}
				else
					i = 0;
			}
		}
		return i;
	}
	

	@Override
	public int hashCode() {
		return Objects.hash(name, customer, count);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (count != other.count)
			return false;
		if (customer == null) {
			if (other.customer != null)
				return false;
		} else if (!customer.equals(other.customer))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return (other.name.equals(this.name) && other.customer.equals(this.customer) && other.count == this.count);
	}

	@Override
	public String toString() {
		return this.name + ": " + this.customer + ": " + this.count;
	}
	
	
	
}
