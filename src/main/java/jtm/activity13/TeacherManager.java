package jtm.activity13;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class TeacherManager {

	protected Connection conn;


	public TeacherManager() throws SQLException, ClassNotFoundException {
		// TODO #1 When new TeacherManager is created, create connection to the
		// database server:
		// url =
		// "jdbc:mysql://localhost/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8"
		// user = "root"
		// pass = ""
		// Hints:
		// 1. Do not pass database name into url, because some statements
		// for tests need to be executed server-wise, not just database-wise.
		// 2. Set AutoCommit to false and use conn.commit() where necessary in
		// other methods
		
		conn = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/?autoReconnect=true&serverTimezone=UTC&characterEncoding=utf8", "root","" );
		conn.setAutoCommit(false);
	}
	
	
	/**
	 * Returns a Teacher instance represented by the specified ID.
	 * 
	 * @param id the ID of teacher
	 * @return a Teacher object
	 * @throws SQLException 
	 */
	public Teacher findTeacher(int id) {
		// TODO #2 Write an sql statement that searches teacher by ID.
		// If teacher is not found return Teacher object with zero or null in
		// its fields!
		// Hint: Because default database is not set in connection,
		// use full notation for table "database_activity.Teacher"
		
/*		Teacher specificTeacher = new Teacher();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM database_activity.Teacher WHERE id = \"" + id + "\";");
		conn.commit();
		if (rs.next()) {
			specificTeacher = new Teacher(rs.getInt("id"), rs.getString("firstName"), rs.getString("lastName"));
		}
		return specificTeacher;
		}
*/
		Teacher specificTeacher = new Teacher(0, null, null);
		try {
			String sql = "SELECT * FROM database_activity.Teacher WHERE id = ?";
			PreparedStatement prepStatement = conn.prepareStatement(sql);
			prepStatement.setInt(1, id);
			ResultSet rs = prepStatement.executeQuery();
			if (rs.next()) 
				specificTeacher = new Teacher(rs.getInt(1), rs.getString(2), rs.getString(3));
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return specificTeacher;
}
	
	/**
	 * Returns a list of Teacher object that contain the specified first name and
	 * last name. This will return an empty List of no match is found.
	 * 
	 * @param firstName the first name of teacher.
	 * @param lastName  the last name of teacher.
	 * @return a list of Teacher object.
	 * @throws SQLException 
	 */
	public List<Teacher> findTeacher(String firstName, String lastName) {
		// TODO #3 Write an sql statement that searches teacher by first and
		// last name and returns results as ArrayList<Teacher>.
		// Note that search results of partial match
		// in form ...like '%value%'... should be returned
		// Note, that if nothing is found return empty list!
		List<Teacher> listOfTeachers = new ArrayList<Teacher>();
		Teacher specificTeacher = new Teacher();
		try {
			String sql = "SELECT * FROM database_activity.Teacher WHERE firstName like ? AND lastName like ?";
			PreparedStatement prepStatement = conn.prepareStatement(sql);
			prepStatement.setString(1, "%" + firstName + "%");
			prepStatement.setString(2, "%" + lastName + "%");
			ResultSet rs = prepStatement.executeQuery();
			while (rs.next()) {
				specificTeacher = new Teacher(rs.getInt(1), rs.getString(2), rs.getString(3));
				listOfTeachers.add(specificTeacher);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listOfTeachers;
	}

	/**
	 * Insert an new teacher (first name and last name) into the repository.
	 * 
	 * @param firstName the first name of teacher
	 * @param lastName  the last name of teacher
	 * @return true if success, else false.
	 */

	public boolean insertTeacher(String firstName, String lastName) {
		// TODO #4 Write an sql statement that inserts teacher in database.
		try {
			String sqlInsert = "INSERT INTO database_activity.Teacher (firstname, lastname) VALUES (?, ?)";
			PreparedStatement prepStatement = conn.prepareStatement(sqlInsert);
			prepStatement.setString(1, firstName);
			prepStatement.setString(2, lastName);
			prepStatement.executeUpdate();
			conn.commit();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	/**
	 * Insert teacher object into database
	 * 
	 * @param teacher
	 * @return true on success, false on error (e.g. non-unique id)
	 */
	public boolean insertTeacher(Teacher teacher) {
		// TODO #5 Write an sql statement that inserts teacher in database.
		try {
			String sqlInsert = "INSERT INTO database_activity.Teacher (id, firstname, lastname) VALUES (?, ?, ?)";
			PreparedStatement prepStatement = conn.prepareStatement(sqlInsert);
			prepStatement.setInt(1, teacher.getId());
			prepStatement.setString(2, teacher.getFirstName());
			prepStatement.setString(3, teacher.getLastName());
			if (prepStatement.executeUpdate() > 0) {
				conn.commit();
				return true;
			} else
				return false;
		} catch (SQLException e) {
			System.err.println(e);
			return false;
		}
		
	}

	/**
	 * Updates an existing Teacher in the repository with the values represented by
	 * the Teacher object.
	 * 
	 * @param teacher a Teacher object, which contain information for updating.
	 * @return true if row was updated.
	 */
	public boolean updateTeacher(Teacher teacher) {
		boolean status = false;
		// TODO #6 Write an sql statement that updates teacher information.
		try {
			String sqlUpdate = "UPDATE database_activity.Teacher set firstname = ?, lastname = ? WHERE id = ?";
			PreparedStatement prepStatement = conn.prepareStatement(sqlUpdate);
			prepStatement.setInt(3, teacher.getId());
			prepStatement.setString(1, teacher.getFirstName());
			prepStatement.setString(2, teacher.getLastName());
			if (prepStatement.executeUpdate() > 0) {
				conn.commit();
				return true;
			} else 
				return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Delete an existing Teacher in the repository with the values represented by
	 * the ID.
	 * 
	 * @param id the ID of teacher.
	 * @return true if row was deleted.
	 */
	public boolean deleteTeacher(int id) {
		// TODO #7 Write an sql statement that deletes teacher from database.
		try {
			String sqlDelete = "DELETE FROM database_activity.Teacher WHERE id = ?";
			PreparedStatement prepStatement = conn.prepareStatement(sqlDelete);
			prepStatement.setInt(1, id);
			if (prepStatement.executeUpdate() > 0) {
				conn.commit();
				return true;
			} else 
				return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public void closeConnecion() {
		// TODO Close connection to the database server and reset conn object to null
		try {
			if (conn != null)
				conn.close();
			conn = null;
		} catch (Exception e) {
		System.err.println(e);
		}
	}

	

}

