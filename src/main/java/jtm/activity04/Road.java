package jtm.activity04;

public class Road {
	private String from; // Start point
	private String to; // End point
	private int distance; // distance in km
	
	/*- TODO #1
	 * Select menu Source â€” Generate Constructor using Fields....  
	 * and create constructor which sets from, to and distance
	 * values of the newly created object
	 */
	
	/**
	 * @param from
	 * @param to
	 * @param distance
	 */
	public Road(String from, String to, int distance) {
		this.from = from;
		this.to = to;
		this.distance = distance;
	}

	

	/*- TODO #2
	 * Create constructor without parameters, which sets  empty
	 * values or 0 to all object properties
	 */
	public Road() {
		this.from = "";
		this.to = "";
		this.distance = 0;
	}


	/*- TODO #3
	 * Select menu: Source â€” Generate getters and Setters...
	 * and generate public getters/setters for distance, from and to
	 * fields
	 */
	
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	/*- TODO #4
	 * Select menu: Source â€” Generate toString()...
	 * and implement this method, that it returns String in form:
	 * "From â€” To, 00km",
	 * where "From" is actual from point, "To" â€” actual to point and
	 * 00 is actual length of the road
	 * Note that â€” is not dash ("minus key" in jargon), but m-dash!
	 * See more at: https://en.wikipedia.org/wiki/Dash
	 */
	@Override
	public String toString() {
		return from + " — " + to + ", " + distance + "km";
	}

	public static void main(String[] args) {
		Road myroad = new Road("Rīga", "Liepāja", 218);
		Road myroad2 = new Road();
		System.out.println(myroad.toString());
		myroad.setDistance(218); // cits veids
		myroad.setTo("Liepāja"); // cits veids
		myroad.setFrom("Rīga"); // cits veids
		
		
	}
	
}
