package abstraction;

public abstract class Shape {
	
	abstract void draw();
	
	public static void main(String[] args) {
		Shape s = new Circle();
		s.draw();
	}
	
	
}

class Rectangle extends Shape {
	@Override
	void draw() {
		System.out.println("Draving the rectangle");
		
	}
}

class Circle extends Shape {
	@Override
	void draw() {
		System.out.println("Draving the circle");
		
	}
}
